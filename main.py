import tkinter
window = tkinter.Tk()
window.geometry("255x50")
window.title("Clicker Window")
clicks = tkinter.IntVar()

scores = {
	"-1":"-1: Hacker",
	"1":"1: START!",
	"10": "10: Beginner",
	"13": "13: Unlucky?",
	"22": "Catch 22: You have better things to do; you spent so much time on it.",
	"25": "25: 0.8069758011 away from the root of all evil",
	"42": "42: Answer to life, the universe, everything.",
	"37": "37: Objectively the funniest number",
	"50": "50: Committed",
	"69": "69: !",
	"100": "100: So much free time",
	"137": "137: Electromagnetism (electron's charge), relativity (speed of light), Planck's constant.",
	"700": "700: The number of church-bell tolls a Rhode Island man had to endure in one week, according to his lawsuit against said church.",
	"78": "78: The number of antiriot vehicles bought by German police. During a public exhibition, one of the 33-ton vehicles—which was advertised as withstanding bricks, stones, and Molotov cocktails—was damaged by tennis balls, eggs, and plastic bottles filled with water.",
	"500": "500: 50%",
	"705": "705: Keep persisting...",
	"999": "999: One more...",
	"1001": "1001: To infinity and beyond..."}

def onclick(event=None):
	clicks.set(clicks.get() + 1)
	if clicks.get() in scores.keys(): print(scores[str(clicks.get())])
	if clicks.get() == 1000:
		win = tkinter.Tk()
		win.geometry("650x90")
		win.title("WINNER")
		label = tkinter.Label(win, text="YOU WIN THE CLICKER GAME!", font=('Helvetica', 32)).pack()
		label2 = tkinter.Label(win, text="You can keep going, tho.", font=('Helvetica', 16)).pack()


w = tkinter.Message(window, textvariable=clicks).pack()
btn = tkinter.Button(window, text="Click", command=onclick).pack()

window.mainloop()